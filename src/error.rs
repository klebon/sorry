use std::error::Error as ErrorTrait;
use std::fmt::{Display, Formatter};
use std::fmt::Result as FmtResult;
use std::io::Error as IOError;

use capstone::Error as CsError;

use nix::Error as NixError;

#[derive(Debug, PartialEq, Clone)]
pub enum Error {
    BreakpointError,
    NoBreakpoint(u64),
    PtraceError(String),
    BufferOutOfBounds(usize, usize),
    NixError(NixError),
    CapstoneError(CsError),
    BufferNotAllocated,
    IOError(i32),
    CustomError(String)
}

pub enum ErrorType {
    CouldNotResume,
    CouldNotWait,
    CouldNotGetRegs,
    LibcNotFound,
    SymbolNotFound(String)
}

pub fn make_error(e: ErrorType) -> Error {
    let msg = match e {
        ErrorType::CouldNotResume =>
            "Could not resume process execution".to_string(),
        ErrorType::CouldNotWait =>
            "Failed waiting for process stop".to_string(),
        ErrorType::CouldNotGetRegs =>
            "Could not get registers".to_string(),
        ErrorType::LibcNotFound =>
            "LibC could not be found".to_string(),
        ErrorType::SymbolNotFound(name) =>
            format!("The symbol {} could not be found", name)
    };

    Error::CustomError(msg)
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match self {
            Error::BreakpointError => write!(f, "Breakpoint error"),
            Error::NoBreakpoint(a) => {
                write!(f, "No breakpoint at address {}", a)
            }
            Error::PtraceError(s) => {
                write!(f, "Ptrace error: {}", s)
            }
            Error::BufferOutOfBounds(got, exp) => {
                write!(f, "Index out of bounds: expected {}, got {}", exp, got)
            }
            Error::NixError(err) => {
                write!(f, "Nix error: {}", err)
            }
            Error::CapstoneError(s) => {
                write!(f, "Capstone error: {}", s)
            }
            Error::BufferNotAllocated => {
                write!(f, "Buffer not allocated")
            }
            Error::IOError(code) => {
                let s = IOError::from_raw_os_error(*code);
                write!(f, "IO error: {}", s)
            }
            Error::CustomError(s) => {
                write!(f, "Error: {}", s)
            }
        }
    }
}

impl ErrorTrait for Error {
}
