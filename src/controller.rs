use std::cell::UnsafeCell;
use std::ffi::{CStr, CString};
use std::fs;
use std::sync::Arc;

use elf::types::Symbol;

use nix::libc::{c_void, user_regs_struct};

use nix::sys::ptrace::*;
use nix::sys::ptrace::read as ptrace_read;
use nix::sys::ptrace::write as ptrace_write;
use nix::sys::signal::*;
use nix::sys::wait::*;

use nix::unistd::*;

use ::Result;
use error::*;
use map::*;

#[derive(Debug, PartialEq, Eq, Clone, Copy)]
pub enum AllocationStrategy {
    PosixMemAlign,
    Mmap
}

/// Controller that handles the nasty low-level operations such as ptrace calls.
/// The controller is also responsible for the process launch.
///
/// One should never instanciate their own [`TargetController`], the
/// corresponding [`TargetProcess`] already has one accessible from
/// [`TargetProcess::get_controller`].
///
/// ```rust
/// use sorry::controller::TargetController;
/// use sorry::process::TargetProcess;
///
/// let executable = "bin/dummy".to_string();
/// let args = vec!["bin/dummy".to_string()];
/// let target = TargetProcess::new(executable, args);
/// target.start();
/// target.controller_do( |ctrl| {
///     ctrl.resume(false);
///     Ok(())
/// });
/// ```
///
/// [`TargetController`]: struct.TargetController.html
/// [`TargetProcess`]: ../process/struct.TargetProcess.html
/// [`TargetProcess::get_controller`]:
/// ../struct.TargetProcess.html#method.get_controller
pub struct TargetController {
    pub(in super) pid: UnsafeCell<Pid>,
    executable: String,
    arguments: Vec<String>,
    load_address: UnsafeCell<u64>,
    memory_map: UnsafeCell<Vec<Arc<MemoryMapEntry>>>,
    pub allocation_strategy: AllocationStrategy
}

unsafe impl Sync for TargetController {}

impl TargetController {
    /// Create a new target controller from its main command line.
    pub fn new<S, Sargs>(filename: S, args: Vec<Sargs>) -> Self
    where S: AsRef<str>, Sargs: AsRef<str> {
        let args_s = args.into_iter()
            .map( |s| s.as_ref().to_string() )
            .collect();

        TargetController {
            pid: UnsafeCell::new(Pid::from_raw(0)),
            executable: filename.as_ref().to_string(),
            arguments: args_s,
            load_address: UnsafeCell::new(0),
            memory_map: UnsafeCell::new(Vec::with_capacity(10)),
            allocation_strategy: AllocationStrategy::PosixMemAlign
        }
    }

    /// Get the target's PID.
    pub fn get_pid(&self) -> Pid {
        unsafe { (*self.pid.get()).clone() }
    }

    /// Get the target executable filename.
    pub fn get_executable_name(&self) -> &str {
        self.executable.as_ref()
    }

    pub fn get_memory_map(&self) -> &Vec<Arc<MemoryMapEntry>> {
        unsafe { &*self.memory_map.get() }
    }

    /// Get the absolute load address of the process.
    ///
    /// This value is used to calculate addresses in the process using their
    /// offset in the ELF file.
    ///
    /// ```rust
    /// use sorry::controller::TargetController;
    /// use sorry::process::TargetProcess;
    ///
    /// let executable = "bin/dummy".to_string();
    /// let args = vec!["bin/dummy".to_string()];
    /// let mut target = TargetProcess::new(executable, args);
    /// target.start();
    ///
    /// let main_address = target.get_main_function_offset()
    ///     .map( |main_offset| {
    ///     target.get_controller().get_absolute_load_address() + main_offset
    /// });
    /// ```
    pub fn get_absolute_load_address(&self) -> u64 {
        unsafe { *self.load_address.get() }
    }

    fn execute_program(&self) {
        let arguments: &Vec<String> = &self.arguments;
        let args: Vec<CString> = arguments.into_iter().map({ |arg|
                CString::new(arg.as_bytes()).unwrap_or(CString::default())
            }).collect();

        let c_args: Vec<&CStr> = args.iter()
            .map( |arg| arg.as_c_str() )
            .collect();

        let exec_bytes = self.executable.as_bytes();
        if let Ok(exec) = CString::new(exec_bytes) {
            let e: CString = exec;
            let _ = execv(&e, c_args.as_slice());
        }

        // Starting here, execv has failed.
        error!("Could not execute program \"{}\"", self.executable);
    }

    /// Start the program and stop it at its entry point.
    /// If the process is started successfully, this function returns its pid.
    pub fn start(&self) -> Option<Pid> {
        match fork() {
            Ok(ForkResult::Parent { child, .. }) => {
                info!("Successfully forked (pid = {})", child);

                match waitpid(child, None) {
                    Ok(WaitStatus::Exited(_, _)) => {
                        let exec = &self.executable;
                        error!("Could not execute program {}", exec);
                        return None
                    }

                    Ok(WaitStatus::Stopped(pid, Signal::SIGTRAP))=> {
                        unsafe { *self.pid.get() = pid; }
                        if let Err(_) = self.load_map_file() {
                            return None;
                        }

                        return setoptions(pid, Options::PTRACE_O_TRACEEXEC).ok()
                            .map( |_| pid );
                    }

                    Ok(status) => {
                        error!("Unknown status received: {:?}", status);
                    }

                    Err(e) => {
                        error!("Could not execute program {}: {}",
                               self.executable, e);
                    }
                }

            }

            Ok(ForkResult::Child) => {
                match traceme() {
                    Ok(_) => self.execute_program(),
                    Err(e) => error!("Could not trace child process: {}", e)
                }
            }

            Err(e) => {
                error!("Could not fork: {}", e);
            }
        }

        return None;
    }

    /// Wait a signal from the child process.
    pub fn wait(&self, options: Option<WaitPidFlag>) -> Result<WaitStatus> {
            waitpid(self.get_pid(), options)
                .or_else( |err| Err(Error::NixError(err)) )
    }

    /// Resume the process execution. If the syscall parameter is set, the
    /// process will stop at the next system call.
    pub fn resume(&self, system_call: bool) -> Result<()> {
        if system_call {
            syscall(self.get_pid(), None)
                .or_else( |err| Err(Error::NixError(err)) )
        }

        else {
            cont(self.get_pid(), None).or_else( |e| Err(Error::NixError(e)) )
        }
    }

    /// Stop the process execution using the given signal.
    pub fn stop(&self, signal: Signal) -> Result<WaitStatus> {
        while self.is_running()? {
            nix::sys::signal::kill(self.get_pid(), signal)
                .map_err( |err| Error::NixError(err) )?;
        }

        self.wait(None)
    }

    pub fn is_running(&self) -> Result<bool> {
        let filepath = format!("/proc/{}/status", self.get_pid());
        let contents = fs::read_to_string(filepath).map_err( |err| {
            let code = err.raw_os_error().unwrap();
            Error::IOError(code)
        })?;

        let state = contents.lines()
            .nth(2).unwrap()
            .split_whitespace()
            .nth(1).unwrap();

        Ok(["R", "S"].contains(&state))
    }

    /// Resume the process execution for only one step.
    pub fn singlestep_resume(&self) -> Result<()> {
        step(self.get_pid(), None).or_else( |e| Err(Error::NixError(e)) )
    }

    /// Read a word from the target process.
    pub fn read_word(&self, offset: u64) -> Result<u64> {
        let addr_ptr = offset as *mut c_void;
        let msg = format!(
            "Could not read at address {:?} in process (PID = {})",
            addr_ptr, self.get_pid());

        ptrace_read(self.get_pid(), addr_ptr)
            .map( |word| word as u64 )
            .or(Err(Error::PtraceError(msg)))
    }

    /// Write a word to the target process.
    pub fn write_word(&self, word: u64, offset: u64) -> Result<()> {
        let addr_ptr = offset as *mut c_void;
        let word_ptr = word as *mut c_void;
        let msg = format!(
            "Could not write at address {:?} in process (PID = {})",
            addr_ptr, self.get_pid());

        ptrace_write(self.get_pid(), addr_ptr, word_ptr)
            .or(Err(Error::PtraceError(msg)))
    }

    /// Get register values from the target process.
    pub fn get_registers(&self) -> Result<user_regs_struct> {
        getregs(self.get_pid())
            .or_else( |err| {
                let msg = format!("Could not get the registers: {:?}", err);
                Err(Error::PtraceError(msg))
            })
    }

    /// Set register values to the target process.
    pub fn set_registers(&self, regs: user_regs_struct) -> Result<()> {
        setregs(self.get_pid(), regs)
            .or(Err(Error::PtraceError("Could not set registers".to_string())))
    }

    pub(crate) fn put_trap_instruction(&self, addr: u64) -> Result<()> {
        let addr_ptr = addr as *mut c_void;
        let trap_ptr = 0xCC as *mut c_void;

        ptrace_write(self.get_pid(), addr_ptr, trap_ptr)
            .or_else( |err| Err(Error::NixError(err)) )
    }

    /// Calculate the absolute load address of the process.
    // TODO: Rewrite using the memory map.
    pub fn calculate_load_address(&self) {
        let exec_name = self.executable.clone();

        self.find_memory_map_entry( |entry| {
            entry.filename.as_ref()
                .map( |name| name.contains(&exec_name) )
                .unwrap_or(false)
        }).map( |e| e.start_addr ).map( |addr| {
            unsafe { *self.load_address.get() = addr; }
        }).or_else( || {
            warn!("No entry named {} found in map file.", &self.executable);
            None
        });
    }

    /// Load the `/proc/{pid}/maps` file into the memory map.
    /// This step is mandatory to manipulate the libraries loaded by the process
    /// such as libc.
    pub fn load_map_file(&self) -> Result<()> {
        unsafe { &mut (*self.memory_map.get()).clear(); }
        let filename = format!("/proc/{}/maps", self.get_pid());
        fs::read_to_string(filename).map( |contents| {
            for line in contents.lines() {
                if let Some(entry) = MemoryMapEntry::from_str(line) {
                   unsafe {
                       &mut (*self.memory_map.get()).push(Arc::new(entry));
                   }
                }
            }
        }).map_err(|err| {
            let msg = format!("{:?}", err);
            Error::CustomError(msg)
        })
    }

    /// Get a memory map entry from its filename and its permissions if needed.
    pub fn find_memory_map_entry<P>(&self, pred: P)
        -> Option<Arc<MemoryMapEntry>>
        where P: Fn(&&MemoryMapEntry) -> bool {
        unsafe { &*self.memory_map.get() }.iter()
            .find( |rc| {
                let entry: &MemoryMapEntry = *rc;
                pred(&entry)
            }).map ( |rc| rc.clone() )
    }

    /// Get the memory-map entry for libc if it exists.
    pub fn get_libc(&self) -> Option<Arc<MemoryMapEntry>> {
        self.find_memory_map_entry( |entry| {
            entry.filename.as_ref().map( |ref filename| {
                filename.contains("libc-") ||
                filename.contains("libc.")
            }).unwrap_or(false) &&
            entry.permissions.is_readable()
        })
    }

    /// Get the range of addresses in the target process address space that
    /// corresponds to the libc.
    pub fn get_libc_address_range(&self) -> Option<(u64, u64)> {
        self.get_libc().map( |libc| (libc.start_addr, libc.end_addr) )
    }

    /// Retrieve a symbol from the LibC.
    pub fn get_libc_symbol(&self, name: &str) -> Result<Symbol> {
        let libc = self.get_libc()
            .ok_or(make_error(ErrorType::LibcNotFound))?;

        libc.elf.as_ref().and_then( |ref elf| {
            for section_name in [".symtab", ".dynsym"] {
                if let Some(section) = elf.get_section(section_name) {
                    return libc.find_symbol(name, section);
                }
            }

                None
        }).ok_or(make_error( ErrorType::SymbolNotFound(name.to_string())))
    }

    /// Call a function from the LibC in the target process.
    pub fn remote_call_libc_function(&self, name: &str, regs: user_regs_struct)
        -> Result<user_regs_struct> {
            let sym = self.get_libc_symbol(name)?;

            let mut regs_mut = regs;

            let (start, _) = self.get_libc_address_range()
                .ok_or(make_error(ErrorType::LibcNotFound))?;

            let reg_save = self.get_registers()?;
            let word_save = self.read_word(regs.rip)?;

            self.inject_abs_call(regs.rip)?;
            regs_mut.rax = start + sym.value;

            self.set_registers(regs_mut)?;

            self.resume(false)
                .or(Err(make_error(ErrorType::CouldNotResume)))?;
            self.wait(None)
                .or(Err(make_error(ErrorType::CouldNotWait)))?;

            let regs_res = self.get_registers();

            self.set_registers(reg_save)?;
            self.write_word(word_save, regs.rip)?;
            regs_res.or(Err(make_error(ErrorType::CouldNotGetRegs)))
        }

    /// Inject the code to call a function whose address is stored in `%rax`.
    pub fn inject_abs_call(&self, rip: u64) -> Result<()> {
        let call_abs = [0xff, 0xd0, 0xcc, 0x90, 0x90, 0x90, 0x90, 0x90];
        self.write_word(u64::from_ne_bytes(call_abs), rip)
    }
}

