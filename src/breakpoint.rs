use ::Result;
use ::controller::TargetController;

/// The breakpoint mode determines its behavior when the breakpoint is
/// triggered. If it is a one-shot, the breakpoint is not set back when the
/// execution resumes. Otherwise, the breakpoint is put back in order to be
/// triggered if reached again.
#[derive(Clone, Copy, PartialEq, Eq)]
pub enum Mode {
    OneShot,
    Normal
}

/// Breakpoint within the target process.
#[derive(Clone)]
pub struct Breakpoint {
    /// Address of the breakpoint.
    address: u64,
    /// Backup of the instruction replaced by a trap to set the breakpoint.
    backup: u64,
    /// Breakpoint mode.
    mode: Mode
}

impl Breakpoint {
    pub fn new(address: u64, mode: Mode) -> Self {
        Breakpoint {
            address: address,
            backup: 0, // The breakpoint is not set so there is no backup.
            mode: mode
        }
    }

    /// Set the breakpoint into the target process.
    pub fn set(&mut self, ctrl: &TargetController) -> Result<()> {
        let word = ctrl.read_word(self.address)?;
        self.backup = word;

        ctrl.put_trap_instruction(self.address)
    }

    /// Move the instruction pointer to the breakpoint location and write back
    /// the original code erased by the trap instruction.
    pub fn clean_code(&self, ctrl: &TargetController) -> Result<()> {
        let mut regs = ctrl.get_registers()?;
        regs.rip = self.address;

        ctrl.set_registers(regs)?;
        ctrl.write_word(self.backup, self.address)
    }

    /// Resume execution after the breakpoint has been handled. If the
    /// breakpoint is not a one-shot, it is properly put back.
    pub fn resume_execution(&mut self, ctrl: &TargetController) -> Result<()> {
        if ! self.is_one_shot() {
            ctrl.singlestep_resume()?;
            self.set(ctrl)?;
        }

        ctrl.resume(false)
    }

    pub fn get_address(&self) -> u64 {
        self.address
    }

    pub fn is_one_shot(&self) -> bool {
        self.mode == Mode::OneShot
    }
}
