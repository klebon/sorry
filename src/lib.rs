//! Dynamic Binary Analysis Framework based on the ptrace system call.
//!
//! This crate uses several libraries to do its job:
//!
//! * [nix](https://docs.rs/nix): Unix system calls
//! * [elf](https://docs.rs/elf): ELF file parsing
//! * [capstone](https://docs.rs/capstone): Capstone disassembler

extern crate capstone;
//extern crate keystone;
extern crate elf;
extern crate nix;

extern crate regex;

#[macro_use]
extern crate log;
extern crate simplelog;

pub mod assembly;
pub mod buffer;
pub mod breakpoint;
pub mod controller;
pub mod error;
pub mod map;
pub mod process;

pub use controller::*;
pub use process::*;

use error::Error;
use simplelog::*;

use std::result::Result as StdResult;

pub type Result<T> = StdResult<T, Error>;

#[allow(dead_code)]
pub fn init_log_system() {
    CombinedLogger::init(
        vec![
            SimpleLogger::new(LevelFilter::Error, Config::default()),
            SimpleLogger::new(LevelFilter::Warn, Config::default()),
            SimpleLogger::new(LevelFilter::Info, Config::default()),
            SimpleLogger::new(LevelFilter::Debug, Config::default())
        ]
    ).unwrap()
}
