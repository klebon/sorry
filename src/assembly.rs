use capstone::arch::x86::*;
use capstone::Insn;
use capstone::prelude::*;

/// Create a Capstone object with the correct configuration to work with
/// x86_64 binaries.
pub fn create_capstone_object() -> Capstone {
    Capstone::new()
        .x86()
        .mode(ArchMode::Mode64)
        .syntax(ArchSyntax::Att)
        .detail(true)
        .build()
        .expect("Failed to create Capstone object")
}

/// Check if the given instruction is a control-flow instruction.
pub fn instr_is_cti(instr: &Insn) -> bool {
    let cti = vec![
        "jmp", "call", "jle", "jl", "jge", "jg", "jz", "jnz", "jbe", "jb",
        "ja", "jae", "jcxz", "jecxz", "jno", "jnp", "jns", "jnz", "jo",
        "jp", "js", "retq", "jmpq", "callq"
    ];

    cti.contains(&instr.mnemonic().unwrap_or(""))
}

