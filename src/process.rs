use std::fs::read_to_string;
use std::string::String;
use std::sync::Arc;

use capstone::arch::*;
use capstone::Error as CsError;
use capstone::Insn;

use elf::File as ElfFile;
use elf::Section;
use elf::types::*;

use nix::sys::ptrace::attach;
use nix::sys::wait::*;
use nix::unistd::*;

use ::Result;
use controller::*;
use assembly::{create_capstone_object, instr_is_cti};
use error::Error;

/// Target process instrumented by the Sorry client.
///
/// This is the main data structure of this library. It enables the user to
/// instrument the execution of a process. A [`TargetProcess`] can be created
/// through the method [`new`]:
///
/// ```rust
/// use sorry::process::TargetProcess;
///
/// let executable = "bin/dummy".to_string();
/// let args = vec!["bin/dummy", "arg1", "arg2"]
///     .iter()
///     .map( |s| s.to_string())
///     .collect();
/// let target = TargetProcess::new(executable, args);
/// ```
///
/// In order to run the program, the method [`start`] must be called. This
/// method starts the process and stops it at its entry point. Note that at this
/// point of a process execution, no library is loaded, not even the C library!
///
/// [`TargetProcess`]: struct.TargetProcess.html
/// [`new`]: struct.TargetProcess.html#method.new
/// [`start`]: struct.TargetProcess.html#method.start
pub struct TargetProcess {
    controller: Arc<TargetController>,
    elf: ElfFile
}

impl TargetProcess {
    /// Create a new target process given an executable file and arguments.
    /// The first argument must be the executable filename.
    pub fn new<S, Sargs>(filename: S, args: Vec<Sargs>) -> Self
    where S: AsRef<str>, Sargs: AsRef<str> {
        let exec = std::fs::canonicalize(filename.as_ref())
            .map( |path| path.to_string_lossy().to_string() )
            .unwrap_or(filename.as_ref().to_string());

        let elf = ElfFile::open_path(&exec).unwrap_or(ElfFile::new());
        let ctrl = TargetController::new(&exec, args);
        TargetProcess {
            controller: Arc::new(ctrl),
            elf: elf
        }
    }

    /// Attach to a target process using its PID.
    pub fn from_pid(pid: i32) -> Result<Self> {
        attach(Pid::from_raw(pid)).or_else( |err| Err(Error::NixError(err)) )?;

        // Error conversion function from std::io::Error to error::Error.
        fn from_io_error<T>(err: std::io::Error) -> Result<T> {
            let code = err.raw_os_error().unwrap_or_default();
            Err(Error::IOError(code))
        }

        let cmdline: String = format!("/proc/{}/cmdline", pid);
        let cmd = read_to_string(cmdline).or_else(from_io_error)?;

        let argv: Vec<String> = cmd.split('\0')
            .filter_map( |s| {
                if ! s.is_empty() {
                    Some(s.to_string())
                } else { None }} )
            .collect();
        let exec = std::fs::canonicalize(argv[0].clone())
            .map( |path| path.to_string_lossy().to_string() )
            .unwrap_or(argv[0].clone() );

        let target = TargetProcess::new(exec, argv);
        unsafe { *(target.controller.pid.get()) = Pid::from_raw(pid); }
        target.controller.load_map_file()?;
        target.controller.calculate_load_address();

        Ok(target)
    }

    /// Get the pid of the target process.
    /// If the process has not been started yet, this function returns 0.
    pub fn get_pid(&self) -> Pid {
        self.controller.get_pid()
    }

    /// Get the executable filename.
    pub fn get_executable_name(&self) -> &str {
        self.controller.get_executable_name()
    }

    /// Start the program and stop it at its entry point.
    /// If the process is started successfully, this function returns its pid.
    pub fn start(&self) -> Option<Pid> {
        self.controller.start().and_then( |pid| {
            self.controller.calculate_load_address();
            Some(pid)
        })
    }

    pub fn wait(&self, options: Option<WaitPidFlag>) -> Result<WaitStatus> {
        let status = waitpid(self.get_pid(), options);
        return status.or_else( |err| Err(Error::NixError(err)) );
    }

    /// Get the target program's ELF metadata.
    pub fn get_target_metadata(&self) -> &ElfFile {
        &self.elf
    }

    pub fn get_controller(&self) -> Arc<TargetController> {
        self.controller.clone()
    }

    /// Perform actions with the inner [`TargetController`].
    ///
    /// [`TargetController`]: struct.TargetController.html
    pub fn controller_do<F>(&self, func: F) -> Result<()>
        where F: Fn(&TargetController) -> Result<()> {
        func(&*self.controller)
    }

    /// Perform actions with the mutable inner [`TargetController`].
    ///
    /// [`TargetController`]: struct.TargetController.html
    pub fn controller_mut_do<F>(&mut self, func: F) -> Result<()>
    where F: Fn(&mut TargetController) -> Result<()> {
        let opt = Arc::get_mut(&mut self.controller);
        if let Some(cell) = opt {
            func(cell)
        }

        else {
            Err(Error::CustomError("Could not borrow controller".to_string()))
        }
    }

    /// Retrieve every symbols of every sections of the target process.
    pub fn get_all_symbols(&self) -> Vec<Symbol> {
        let mut syms = vec![];
        for section in &self.elf.sections {
            let res = self.elf.get_symbols(&section);
            let mut res_vec = res.unwrap_or_default();
            syms.append(&mut res_vec);
        }
        return syms;
    }

    /// Find a symbol by name from the given section.
    pub fn find_symbol(&self, name: &str, sect: &Section) -> Option<Symbol> {
        self.elf
            .get_symbols(sect)
            .map(|syms| syms.into_iter().find( |sym| sym.name == name))
            .unwrap_or(None)
    }

    fn try_get_main_function_from_symtab(&self) -> Result<u64> {
        for section in [".symtab", ".dynsym"].iter() {
            if let Some(symtab) = self.elf.get_section(section) {
                info!("Symbol table found at 0x{:x}", symtab.shdr.offset);
                // Search for a "main" symbol in the symbol table.
                let main_sym = self.find_symbol("main", symtab);
                if let Some(sym) = main_sym {
                    info!("Found main symbol in ELF file at 0x{:x}", sym.value);
                    return Ok(sym.value);
                }

                // If no main symbol exists, the first argument of the function
                // __libc_start_main can be used.
                else {
                    let msg = "No main symbol found in the ELF file";
                    return Err(Error::CapstoneError(CsError::CustomError(msg)));
                }
            }
        }

        let cs = CsError::CustomError("No .symtab section found");
        Err(Error::CapstoneError(cs))
    }

    fn try_get_main_function_from_entry_point(&self) -> Result<u64> {
        info!("No symbol table found, the binary must be stripped.");

        let cs = create_capstone_object();
        if let Some(text) = self.elf.get_section(".text") {
            //let endbr64_offset = skip_endbr64(&text.data);
            let data = &text.data;

            let offset = text.shdr.addr as u64;
            let entry = (self.elf.ehdr.entry - offset) as usize;

            // TODO: Make sure the data vector starts at the entry point and not
            // the section beginning.
            let res = cs.disasm_all(&data[entry..], self.elf.ehdr.entry);
            res.or_else( |err| Err(Error::CapstoneError(err)) )
                .and_then( |instrs| {
                let insvec: Vec<Insn> = instrs.iter().collect();

                let mut idx = 0;
                let max = instrs.len();

                // Iterate until the first control-flow instruction and make
                // sure that last represents the instruction before the
                // basic-block terminator.
                while idx < max && ! instr_is_cti(&insvec[idx]) {
                    idx = idx + 1;
                }

                if idx < max {
                    let last = &insvec[idx - 1];

                    let instr_addr = insvec[idx].address();
                    let detail = cs.insn_detail(&last).unwrap();

                    match detail.arch_detail() {
                        ArchDetail::X86Detail(arch_detail) => {
                            let disp = arch_detail.disp();
                            Ok(((instr_addr as i64) + (disp as i64)) as u64)
                        }
                        _ => {
                            let msg = "Invalid Architecture";
                            let err = CsError::CustomError(msg);
                            Err(Error::CapstoneError(err))
                        }
                    }
                }

                else {
                    let msg = "No basic-block terminator found";
                    let err = CsError::CustomError(msg);
                    Err(Error::CapstoneError(err))
                }
            })
        }

        else {
            let ce = CsError::CustomError("No .text section found");
            Err(Error::CapstoneError(ce))
        }
    }

    /// Get the offset of the main function within the ELF file.
    ///
    /// If possible, the function just reads the value of the symbol "main" in
    /// the .symtab section of the ELF file. If the binary is stripped, then
    /// this function disassembles the basic-block at the entry point and get
    /// the first argument to the `__libc_start_main` function, which is the
    /// address of the main function.
    pub fn get_main_function_offset(&self) -> Result<u64> {
        self.try_get_main_function_from_symtab().or_else( |_|
            self.try_get_main_function_from_entry_point())
    }
}
