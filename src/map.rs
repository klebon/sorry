use std::fmt::{Display, Formatter, Result};

use elf::{File, Section};
use elf::types::Symbol;

use regex::Regex;

/// Unix permissions of a memory-map entry.
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Permissions {
    Permissions(bool, bool, bool)
}

impl Display for Permissions {
    fn fmt(&self, f: &mut Formatter) -> Result {
        let r = if self.is_readable() { "r" } else { "-" };
        let w = if self.is_writable() { "w" } else { "-" };
        let x = if self.is_executable() { "x" } else { "-" };
        write!(f, "{}{}{}", r, w, x)
    }
}

impl Permissions {
    /// Check if the memory-map entry is readable.
    pub fn is_readable(&self) -> bool {
        match self {
            Permissions::Permissions(x, _, _) => *x
        }
    }

    /// Check if the memory-map entry is writable.
    pub fn is_writable(&self) -> bool {
        match self {
            Permissions::Permissions(_, x, _) => *x
        }
    }

    /// Check if the memory-map entry is executable.
    pub fn is_executable(&self) -> bool {
        match self {
            Permissions::Permissions(_, _, x) => *x
        }
    }

    /// Build a [`MemoryMapEntry`] from a &[`str`].
    ///
    /// [`MemoryMapEntry`]: struct.MemoryMapEntry.html
    /// [`str`]: https://doc.rust-lang.org/std/str/
    pub fn from_str(s: &str) -> Self {
        let p = s.as_bytes();
        let r = p[0] == 'r' as u8;
        let w = p[1] == 'w' as u8;
        let x = p[2] == 'x' as u8;

        Permissions::Permissions(r, w, x)
    }
}

fn hex_from_str(s: &str) -> u64 {
    u64::from_str_radix(s, 16).unwrap()
}

/// Representation of an entry in the `/proc/{pid}/maps` file.
/// This struct is used to retrieve symbols from the ELF file associated with
/// the entry (usually a library).
#[derive(Debug)]
pub struct MemoryMapEntry {
    /// Start address of the map entry.
    pub start_addr: u64,
    /// End address of the map entry.
    pub end_addr: u64,
    /// The permissions associated with the entry.
    pub permissions: Permissions,
    /// The name of the ELF file.
    pub filename: Option<String>,
    /// The ELF file if any.
    pub elf: Option<File>
}

impl PartialEq<MemoryMapEntry> for MemoryMapEntry {
    fn eq(&self, other: &Self) -> bool {
        self.start_addr == other.start_addr &&
        self.end_addr == other.end_addr &&
        self.permissions == other.permissions &&
        self.filename == other.filename
    }

    fn ne(&self, other: &Self) -> bool {
        self.start_addr != other.start_addr ||
        self.end_addr != other.end_addr ||
        self.permissions != other.permissions ||
        self.filename != other.filename
    }
}

impl MemoryMapEntry {
    /// Build a [`MemoryMapEntry`] from its string representation.
    ///
    /// [`MemoryMapEntry`]: struct.MemoryMapEntry.html
    pub fn from_str(line: &str) -> Option<Self> {
        let mut words = line.split_whitespace();
        let range = words.next()?;
        let perms = words.next()?;

        words.next();
        words.next();
        words.next();

        let filename = words.next();

        let mut addrs = range.split('-');
        let start = addrs.next()?;
        let end = addrs.next()?;

        let elf = filename.map( |name| {
            File::open_path(&name).unwrap_or(File::new())
        });

        Some(MemoryMapEntry {
            start_addr: hex_from_str(start),
            end_addr: hex_from_str(end),
            permissions: Permissions::from_str(perms),
            filename: filename.map( str::to_string ),
            elf: elf
        })
    }

    /// Find a symbol by name from the given section.
    pub fn find_symbol(&self, name: &str, sect: &Section) -> Option<Symbol> {
        let re = Regex::new(&format!(r"^{}(@@GLIBC_\d+\.\d+\.\d+)?$", name)).unwrap();

        if let Some(elf) = &self.elf {
            elf.get_symbols(sect)
                .map(|syms| syms.into_iter().find( |sym| re.is_match(&sym.name)))
                .unwrap_or(None)
        }

        else {
            None
        }
    }

    /// Retrieve every symbols of every sections of the memory map.
    pub fn get_all_symbols(&self) -> Vec<Symbol> {
        let mut syms = vec![];

        if let Some(elf) = &self.elf {
            for section in &elf.sections {
                let res = elf.get_symbols(&section);
                let mut res_vec = res.unwrap_or_default();
                syms.append(&mut res_vec);
            }

            return syms;
        }

        return vec![];
    }
}

#[cfg(test)]
mod tests {
    use regex::Regex;

    fn regex(name: &str) -> Regex {
        Regex::new(&format!(r"^{}(GLIBC)?", name)).unwrap()
    }

    #[test]
    fn symbol_regex() {
        assert!(regex("posix_memalign").is_match("posix_memalign"));
        assert!(regex("posix_memalign").is_match("posix_memalign@@GLIBC_2.2.5"));
    }
}
