use std::fs::{File, OpenOptions};
use std::io::Result as IOResult;
use std::io::{Seek, SeekFrom, Read, Write};

use nix::unistd::Pid;

use ::Result;
use buffer::Buffer;
use error::Error;

/// A remote buffer is a view of a memory range within the target process. It
/// enables to read and write directly into the target process' address space.
/// However, it does not allocate memory, a [`CodeCache`] must be used instead.
/// The main uses of a remote buffer are to read and write to the process memory
/// or its code.
///
/// [`CodeCache`]: ../codecache/struct.CodeCache.html
#[derive(Clone, Debug)]
pub struct RemoteBuffer {
    pub address: u64,
    pub size: usize,
    pub pid: Pid,
}

impl RemoteBuffer {
    /// Create a new [`RemoteBuffer`]. Nothing is allocated, since there is no
    /// need to.
    pub fn new(pid: Pid, addr: u64, size: usize) -> Self {
        RemoteBuffer {
            address: addr,
            size: size,
            pid: pid,
        }
    }
}

/// Open the /proc/$PID/mem file with either the read or write permission, seek
/// the right position in it (the offset parameter) and apply the given closure
/// on the file.
///
/// This function is used by the implementation of the [`Buffer`] trait for
/// [`RemoteBuffer`]. It encapsulates all the boilerplate code needed by the
/// file to be ready and for error handling.
fn do_with_file<F, R>(remote: &RemoteBuffer, offset: usize, write: bool, f: F)
        -> Result<R> where F : Fn(&mut File) -> IOResult<R> {
    let path = format!("/proc/{}/mem", remote.pid);
    OpenOptions::new()
        .read(! write)
        .write(write)
        .open(path)
        .and_then( |mut memfile| {
            memfile.seek(SeekFrom::Start(remote.address + offset as u64))?;
            f(&mut memfile)
        }).or_else( |err| {
            let code = err.raw_os_error().unwrap_or(0);
            Err(Error::IOError(code))
        })
}

impl Buffer for RemoteBuffer {
    /// Useless, do not call.
    fn allocate(&mut self, _: usize) -> Result<()> {
        Ok(())
    }

    fn read_byte(&self, offset: usize) -> Result<u8> {
        do_with_file(&self, offset, false, |memfile| {
            let mut buf = [0; 1];
            memfile.read_exact(&mut buf)?;
            Ok(buf[0])
        })
    }

    fn read(&self, length: usize, offset: usize) -> Result<Vec<u8>> {
        do_with_file(&self, offset, false, |memfile| {
            let mut buf = Vec::with_capacity(length);
            memfile.take(length as u64).read_to_end(&mut buf)?;
            Ok(buf)
        })
    }

    fn write_byte(&mut self, byte: u8, offset: usize) -> Result<()> {
        do_with_file(&self, offset, true, |memfile| {
            memfile.write(&[byte]).map( |_| {} )
        })
    }

    fn write(&mut self, bytes: &[u8], offset: usize) -> Result<()> {
        do_with_file(&self, offset, true, |memfile| {
            memfile.write(bytes).map( |_| {} )
        })
    }

    fn get_size(&self) -> usize {
        self.size
    }

    fn get_address(&self) -> u64 {
        self.address
    }

    fn make_another(&self) -> Self {
        self.clone()
    }
}
