use std::fs::{File, OpenOptions};
use std::io::{Seek, SeekFrom, Read, Write};
use std::mem::size_of_val;
use std::sync::Arc;

use nix::libc::{PROT_EXEC, PROT_READ, PROT_WRITE};
use nix::unistd::{sysconf, SysconfVar};

use buffer::*;
use buffer::remote::RemoteBuffer;
use controller::{AllocationStrategy, TargetController};
use error::Error;

/// Code cache in the target process.
pub struct CodeCache {
    controller: Arc<TargetController>,
    /// Offset in the target-process address space where the code cache is
    /// stored
    offset: usize,
    /// Length of the code cache
    length: usize,
    /// Tell if the code cache must be deallocated after use.
    must_deallocate: bool
}

impl CodeCache {
    /// Create a new code cache of given length. This function also allocate it
    /// in the target process. Thus, it requires the target process to be
    /// stopped.
    pub fn new(ctrl: Arc<TargetController>, length: usize) -> Result<Self> {
        let mut cc = CodeCache {
            controller: ctrl,
            offset: 0,
            length: length,
            must_deallocate: false
        };

        cc.allocate(length)?;
        Ok(cc)
    }

    pub fn get_offset(&self) -> usize {
        self.offset
    }

    pub fn get_length(&self) -> usize {
        self.length
    }

    pub fn must_deallocate(&mut self, must: bool) {
        self.must_deallocate = must
    }

    /// Call posix_memalign to allocate memory in the target process.
    fn call_posix_memalign(&mut self, size: usize) -> Result<()> {
        self.controller.get_registers().and_then( |mut regs| {
            let buffer_ptr = regs.rsp - size_of_val(&regs.rax) as u64;
            let buffer_save = self.controller.read_word(buffer_ptr)?;

            // Setup the parameters.
            // 1st: pointer to a location to put the result address.
            regs.rdi = buffer_ptr;
            // 2nd: alignment (page size).
            regs.rsi = sysconf(SysconfVar::PAGE_SIZE)
                .or_else( |err| {
                    Err(Error::NixError(err))
                })?.expect("No page size found") as u64;
            // 3rd: buffer size.
            regs.rdx = size as u64;

            let ctrl = &self.controller;
            let res = ctrl.remote_call_libc_function("posix_memalign", regs)?;

            let offset = ctrl.read_word(buffer_ptr)?;
            ctrl.write_word(buffer_save, buffer_ptr)?;

            // The result value should be 0.
            if (res.rax as i64) == 0 {
                self.offset = offset as usize;
                Ok(())
            }

            else {
                let msg = format!(
                    "posix_memalign failed with error code: {:p}",
                    res.rax as *const u64);
                Err(Error::CustomError(msg))
            }
        })
    }

    fn get_correct_address(&self) -> Option<u64> {
        let maps = self.controller.get_memory_map();
        let page = self.controller.get_absolute_load_address() & (0xffff << 32);

        let entry = maps.iter()
            .rev()
            .find( |rc| {
                let entry = *rc;
                entry.start_addr & (0xffff << 32) == page
                //entry.filename == Some("[heap]".to_string())
                    //|| entry.filename == Some(executable.to_string())
            });

        entry.map( |e| e.end_addr + 0x1000 )
    }

    fn call_mmap(&mut self, size: usize) -> Result<()> {
        let ctrl = &self.controller;
        let mut regs = ctrl.get_registers()?;

        //regs.rdi = ctrl.get_absolute_load_address();
        regs.rdi = self.get_correct_address().unwrap_or(0);
        regs.rsi = size as u64;
        regs.rdx = 0x7;
        regs.rcx = 0x32;
        regs.r8 = (-1 as i64) as u64;
        regs.r9 = 0;

        let res = ctrl.remote_call_libc_function("mmap", regs)?;

        if res.rax as i64 != -1 {
            self.offset = res.rax as usize;
            Ok(())
        } else {
            let msg = format!(
                "mmap failed with error code: {:p}",
                res.rax as *const u64);
            Err(Error::CustomError(msg))
        }
    }

    /// Call mprotect to allocate memory in the target process.
    fn call_mprotect(&mut self, prot: i32) -> Result<()> {
        let ctrl = &self.controller;
        let mut regs = ctrl.get_registers()?;
        // Setup the parameters.
        // 1st: the buffer address.
        regs.rdi = self.offset as u64;
        // 2nd: the buffer's length.
        regs.rsi = self.length as u64;
        // 3rd: the protections.
        regs.rdx = prot as u64;

        let res = ctrl.remote_call_libc_function("mprotect", regs)?;

        // The result value should be 0.
        if (res.rax as i64) != 0 {
            let mut regs = ctrl.get_registers()?;
            regs = ctrl.remote_call_libc_function("__errno_location", regs)?;

            let remote = RemoteBuffer::new(ctrl.get_pid(), regs.rax, 4);

            let buf = remote.read(4, 0)?;
            let mut fixed: [u8; 4] = Default::default();
            fixed.copy_from_slice(&buf[0 .. 4]);
            let errno = u32::from_ne_bytes(fixed);

            let msg = format!("mprotect failed with errno code: {}",
                              errno);
            Err(Error::CustomError(msg))
        } else {
            Ok(())
        }
    }

    /// Deallocate the code cache from the target process.
    fn deallocate(&mut self) {
        let _ = self.controller.get_registers().and_then( |mut regs| {
            // Setup the parameters.
            // 1st: the buffer address.
            regs.rdi = self.offset as u64;

            let alloc_strat = self.controller.allocation_strategy;
            if alloc_strat == AllocationStrategy::PosixMemAlign {
                self.controller.remote_call_libc_function("free", regs)?;
            } else {
                self.controller.remote_call_libc_function("munmap", regs)?;
            }
            self.offset = 0;
            Ok(())
        });
    }
}

impl Buffer for CodeCache {
    fn allocate(&mut self, size: usize) -> Result<()> {
        let alloc_strat = self.controller.allocation_strategy;
        if alloc_strat == AllocationStrategy::PosixMemAlign {
            self.call_posix_memalign(size)?;
            self.length = size;
            self.call_mprotect(PROT_READ | PROT_WRITE | PROT_EXEC)
        } else {
            self.controller.load_map_file()?;
            self.call_mmap(size)?;
            self.length = size;
            self.call_mprotect(PROT_READ | PROT_WRITE | PROT_EXEC)
        }
    }

    fn read_byte(&self, offset: usize) -> Result<u8> {
        self.controller.read_word((self.offset + offset) as u64)
            .map( |word| word as u8 )
            .or(Err(Error::CustomError("Could not read byte".to_string())))
    }


    fn read(&self, length: usize, offset: usize) -> Result<Vec<u8>> {
        let mempath = format!("/proc/{}/mem", self.controller.get_pid());
        File::open(mempath).and_then( |mut memfile| {
            let mut buf: Vec<u8> = Vec::with_capacity(length);
            memfile.seek(SeekFrom::Start((self.offset + offset) as u64))?;
            memfile.take(length as u64).read_to_end(&mut buf)?;
            Ok(buf)
        }).or_else( |err| {
            let msg = format!("IO Error: {}", err);
            Err(Error::CustomError(msg.to_string()))
        })
    }

    fn write_byte(&mut self, byte: u8, offset: usize) -> Result<()> {
        let address = (self.offset + offset) as u64;
        self.controller.read_word(address)
            .map( |word| (word & !0xFF) + byte as u64 )
            .and_then( |word| self.controller.write_word(word, address) )
            .or(Err(Error::CustomError("Could not read byte".to_string())))
    }

    fn write(&mut self, bytes: &[u8], offset: usize) -> Result<()> {
        let address = (self.offset + offset) as u64;
        let mempath = format!("/proc/{}/mem", self.controller.get_pid());

        let mut options = OpenOptions::new();
        options.write(true).open(mempath).and_then( |mut memfile| {
            memfile.seek(SeekFrom::Start(address))?;
            memfile.write(bytes).map( |_| () )
        }).or_else( |err| {
            let msg = format!("IO Error: {}", err);
            Err(Error::CustomError(msg.to_string()))
        })

    }

    fn get_size(&self) -> usize {
        self.length
    }

    fn get_address(&self) -> u64 {
        self.offset as u64
    }

    fn make_another(&self) -> Self {
        CodeCache {
            controller: self.controller.clone(),
            offset: 0,
            length: 0,
            must_deallocate: self.must_deallocate
        }
    }
}

/// A code cache, which is allocated on the target-process address space, needs
/// to be freed explicitly using `free`.
impl Drop for CodeCache {
    fn drop(&mut self) {
        if self.offset != 0 && self.must_deallocate {
            self.deallocate();
        }
    }
}
