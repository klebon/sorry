use ::Result;
use error::Error;

/// Trait enabling one to manipulate any kind of byte buffers in a transparent
/// way. This trait is used as a basis for local and remote buffers.
pub trait Buffer {
    /// Allocate a new buffer of the given size.
    fn allocate(&mut self, size: usize) -> Result<()>;

    /// Read a byte from the buffer.
    fn read_byte(&self, offset: usize) -> Result<u8>;

    /// Read many bytes from the buffer.
    fn read(&self, length: usize, offset: usize) -> Result<Vec<u8>>;

    /// Write a byte in the buffer.
    fn write_byte(&mut self, byte: u8, offset: usize) -> Result<()>;

    /// Write many bytes in the buffer.
    fn write(&mut self, bytes: &[u8], offset: usize) -> Result<()>;

    /// Get the buffer's size.
    fn get_size(&self) -> usize;

    /// Get the buffer's address.
    fn get_address(&self) -> u64;

    /// Copy the contents of a buffer into this buffer.
    fn copy_from<B>(&mut self, other: &B) -> Result<()> where B: Buffer {
        let contents = other.read(other.get_size(), 0)?;
        self.write(&contents, 0)
    }

    /// Make another of the same type. The new buffer must then be allocated.
    fn make_another(&self) -> Self;
}

/// Helper trait to factorize error handling.
trait VecBuffer {
    /// Execute a function only if the offset is within bounds.
    fn do_in_bounds<T, F>(&self, offset: usize, func: F) -> Result<T>
        where F: FnOnce() -> T;
}

impl VecBuffer for Vec<u8> {
    fn do_in_bounds<T, F>(&self, offset: usize, func: F) -> Result<T>
            where F: FnOnce() -> T {
        if offset < self.len() {
            Ok(func())
        }

        else {
            Err(Error::BufferOutOfBounds(offset, self.len()))
        }
    }
}

impl Buffer for Vec<u8> {
    /// Create a Vec<u8> of the right size and fills it with a default value.
    fn allocate(&mut self, size: usize) -> Result<()> {
        self.clear();
        self.resize_with(size, Default::default);
        Ok(())
    }

    fn read_byte(&self, offset: usize) -> Result<u8> {
        self.do_in_bounds(offset, || self[offset])
    }

    fn read(&self, length: usize, offset: usize) -> Result<Vec<u8>> {
        self.do_in_bounds(offset + length - 1, || {
            let slice: &[u8] = &self[offset .. offset + length];
            Vec::from(slice)
        })
    }

    fn write_byte(&mut self, byte: u8, offset: usize) -> Result<()> {
        if offset < self.len() {
            self[offset] = byte;
            Ok(())
        }

        else {
            Err(Error::BufferOutOfBounds(offset, self.len()))
        }
    }

    fn write(&mut self, bytes: &[u8], offset: usize) -> Result<()> {
        if offset + bytes.len() <= self.len() {
            let upper_bound = offset + bytes.len();
            self.as_mut_slice()[offset .. upper_bound].copy_from_slice(bytes);
            Ok(())
        }

        else {
            Err(Error::BufferOutOfBounds(offset, self.len()))
        }
    }

    fn get_size(&self) -> usize {
        self.len()
    }

    /// A vector has no address, therefore this function always returns 0.
    fn get_address(&self) -> u64 {
        0
    }

    fn copy_from<B>(&mut self, other: &B) -> Result<()> where B: Buffer {
        let contents = other.read(other.get_size(), 0)?;
        self.resize(contents.len(), 0);
        self.write(&contents, 0)
    }

    fn make_another(&self) -> Self {
        vec![]
    }
}

pub mod codecache;
pub mod remote;
