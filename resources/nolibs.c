int main(void) {
  return 0;
}

void _start(void) {
  main();

  asm("movq $1, %rax;"
      "xor %rbx, %rbx;"
      "int $0x80");
}

