#[macro_use]
extern crate log;
extern crate nix;
extern crate sorry;

#[cfg(test)]
mod test {
    use nix::sys::signal::*;
    use nix::sys::wait::*;

    use sorry::TargetProcess;
    use sorry::breakpoint::*;
    use sorry::buffer::Buffer;
    use sorry::buffer::codecache::CodeCache;

    #[test]
    pub fn test_inject_exit_42() {
        let executable = "resources/no-pie".to_string();
        let args = vec![executable.clone()];

        let target = TargetProcess::new(executable, args);
        let start_res = target.start().map( |_| () );
        assert_eq!(start_res, Some(()));

        let ctrl = target.get_controller();

        let mut brk = Breakpoint::new(0x401126, Mode::OneShot);
        let res_set = brk.set(&ctrl);
        assert_eq!(res_set, Ok(()));

        let resume_res = ctrl.resume(false);
        assert_eq!(resume_res, Ok(()));

        let wait_res = ctrl.wait(None);
        assert_eq!(wait_res, Ok(WaitStatus::Stopped(target.get_pid(),
                                                    Signal::SIGTRAP)));

        let res_clean = brk.clean_code(&ctrl);
        assert_eq!(res_clean, Ok(()));

        let res_load = ctrl.load_map_file();
        assert_eq!(res_load, Ok(()));

        let new_res = CodeCache::new(target.get_controller().clone(), 100);

        let res = new_res.map( |mut cc| {
            info!("CC at {:p}", cc.get_offset() as *const usize);

            // mov $42, %edi
            // mov $60, %rax
            // syscall
            let exit42 = &[
                0xbf, 0x2a, 0, 0, 0,
                0x48, 0xc7, 0xc0, 0x3c, 0, 0, 0,
                0x0f, 0x05
            ];

            let write_res = cc.write(exit42, 0);
            assert_eq!(write_res, Ok(()));

            let regs_res = ctrl.get_registers();
            assert!(regs_res.is_ok());

            let mut regs = regs_res.unwrap();
            regs.rax = cc.get_offset() as u64;

            let res_inject = ctrl.inject_abs_call(regs.rip);
            assert_eq!(res_inject, Ok(()));

            let res_set_regs = ctrl.set_registers(regs);
            assert_eq!(res_set_regs, Ok(()));

            let res_resume = brk.resume_execution(&ctrl);
            assert_eq!(res_resume, Ok(()));

            info!("Code written in the code cache");
            let res = target.wait(None);
            assert_eq!(res, Ok(WaitStatus::Exited(target.get_pid(), 42)));
        });

        assert_eq!(res, Ok(()));
    }
}
