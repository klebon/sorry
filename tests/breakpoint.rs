extern crate nix;
extern crate sorry;

#[cfg(test)]
mod tests {
    use std::path::Path;

    use nix::sys::signal::SIGTRAP;
    use nix::sys::wait::WaitStatus;

    use sorry::breakpoint::{Breakpoint, Mode};
    use sorry::TargetProcess;

    #[test]
    fn test_one_shot_breakpoint() {
        let filename = Path::new("resources/no-pie")
            .to_str().unwrap().to_string();
        let args = vec![Path::new("resources/no-pie")
            .to_str().unwrap().to_string()];

        let target = TargetProcess::new(filename, args);
        target.start().map( |_| {
            let res4 = target.controller_do( |ref ctrl| {
                let mut brk = Breakpoint::new(0x401126, Mode::OneShot);
                let res1 = brk.set(ctrl);
                assert_eq!(res1, Ok(()));

                let res2 = ctrl.resume(false)
                    .and_then( |_| target.wait(None) );
                let pid = ctrl.get_pid();

                assert_eq!(res2, Ok(WaitStatus::Stopped(pid, SIGTRAP)));

                let regs_res = ctrl.get_registers();

                if ! regs_res.is_ok() {
                    let err = regs_res.err().unwrap();
                    assert_ne!(err, err);
                }

                else {
                    let regs = regs_res?;
                    assert_eq!(regs.rip, 0x401127);

                    let res3 = brk.clean_code(ctrl);
                    assert_eq!(res3, Ok(()));

                    let res4 = brk.resume_execution(ctrl)
                        .and_then( |_| target.wait(None) );
                    assert_eq!(res4, Ok(WaitStatus::Exited(ctrl.get_pid(), 0)));
                }

                Ok(())
            });

            assert_eq!(res4, Ok(()));
        }).or_else( || panic!("Target process did not start.") );
    }
}
