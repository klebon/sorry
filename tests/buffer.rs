extern crate nix;
extern crate sorry;

#[cfg(test)]
mod tests {
    use nix::sys::signal::*;
    use nix::sys::wait::*;

    use sorry::TargetProcess;
    use sorry::breakpoint::*;
    use sorry::buffer::*;
    use sorry::buffer::codecache::CodeCache;
    use sorry::buffer::remote::RemoteBuffer;
    use sorry::error::*;

    #[test]
    fn allocate_buffer() {
        let mut buf: Vec<u8> = vec![];
        let res = buf.allocate(10);

        assert!(res.is_ok());
        assert_eq!(buf.len(), 10);
    }

    #[test]
    fn read_byte() {
        let buf: Vec<u8> = vec![1, 2, 3];

        let res = buf.read_byte(1);
        assert_eq!(res, Ok(2));

        let res_oob = buf.read_byte(4);
        assert_eq!(res_oob, Err(Error::BufferOutOfBounds(4, 3)));
    }

    #[test]
    fn read_many_bytes() {
        let buf: Vec<u8> = vec![1, 2, 3, 4, 5];

        let res = buf.read(3, 1);
        assert_eq!(res, Ok(vec![2, 3, 4]));

        let res_null = buf.read(0, 1);
        assert_eq!(res_null, Ok(vec![]));

        let res_oob = buf.read_byte(6);
        assert_eq!(res_oob, Err(Error::BufferOutOfBounds(6, 5)));
    }

    #[test]
    fn write_byte() {
        let mut buf: Vec<u8> = vec![1, 2, 3];
        assert_eq!(buf.write_byte(4, 2), Ok(()));
        assert_eq!(buf, vec![1, 2, 4]);
        assert_eq!(buf.write_byte(8, 3), Err(Error::BufferOutOfBounds(3, 3)));
    }

    #[test]
    fn write_many_bytes() {
        let mut buf: Vec<u8> = vec![1, 2, 3];

        assert_eq!(buf.write(&[4, 8], 1), Ok(()));
        assert_eq!(buf, vec![1, 4, 8]);
        assert_eq!(buf.write(&[4, 8], 3), Err(Error::BufferOutOfBounds(3, 3)));
    }

    #[test]
    fn get_size() {
        let buf: Vec<u8> = vec![1, 2, 3, 4];
        assert_eq!(buf.get_size(), 4);
    }

    #[test]
    fn copy_from_remote_to_codecache() {
        let executable = "resources/no-pie".to_string();
        let args = vec![executable.clone()];

        let target = TargetProcess::new(executable, args);
        target.start();
        let ctrl = target.get_controller();

        let mut brk = Breakpoint::new(0x401126, Mode::OneShot);
        let res_brk_set = brk.set(&ctrl);
        assert_eq!(res_brk_set, Ok(()));

        assert_eq!(ctrl.resume(false), Ok(()));
        let wait_stts = WaitStatus::Stopped(target.get_pid(), Signal::SIGTRAP);
        assert_eq!(target.wait(None), Ok(wait_stts));

        let res_clean = brk.clean_code(&ctrl);
        assert_eq!(res_clean, Ok(()));

        let res_load = ctrl.load_map_file();
        assert_eq!(res_load, Ok(()));

        let remote = RemoteBuffer::new(ctrl.get_pid(), 0x401126, 10);
        let res_cc = CodeCache::new(ctrl.clone(), 10);

        match &res_cc {
            Ok(_) => (),
            Err(e) => panic!("Error {:?}", e)
        }

        let res = res_cc.map( |mut cc| {
            let res_copy = cc.copy_from(&remote);
            assert_eq!(res_copy, Ok(()));

            let from_remote = remote.read(10, 0);
            let from_cc = cc.read(10, 0);

            assert_eq!(from_cc, from_remote);
        });

        assert_eq!(res, Ok(()));
    }
}
