extern crate sorry;

#[cfg(test)]
mod tests {
    use sorry::map::*;

    #[test]
    fn permissions_from_str() {
        let p1 = Permissions::from_str("r-x");
        let p2 = Permissions::from_str("--x");
        let p3 = Permissions::from_str("rwx");
        let p4 = Permissions::from_str("r-xp");

        assert_eq!(p1, Permissions::Permissions(true, false, true));
        assert_eq!(p2, Permissions::Permissions(false, false, true));
        assert_eq!(p3, Permissions::Permissions(true, true, true));
        assert_eq!(p4, Permissions::Permissions(true, false, true));
    }

    #[test]
    fn memory_map_from_str() {
        let line = concat!("7f47aeef1000-7f47aeef3000 r--p",
                           " 00000000 103:02 23858026",
                           "                  /usr/lib/ld-2.28.so");
        let entry = MemoryMapEntry::from_str(line);

        assert_eq!(entry, Some(MemoryMapEntry {
            start_addr: 0x7f47aeef1000,
            end_addr: 0x7f47aeef3000,
            permissions: Permissions::Permissions(true, false, false),
            filename: Some("/usr/lib/ld-2.28.so".to_string()),
            elf: None // Don't actually care about this one now.
        }));
    }
}
