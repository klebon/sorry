#[macro_use]
extern crate log;
extern crate nix;
extern crate sorry;

#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::{Seek, SeekFrom, Read};
    use std::path::Path;

    use nix::sys::signal::*;
    use nix::sys::wait::*;

    use sorry::breakpoint::*;
    use sorry::buffer::Buffer;
    use sorry::buffer::codecache::*;
    use sorry::error::Error;
    use sorry::TargetProcess;

    fn init_target() -> TargetProcess {
        let filename = Path::new("resources/no-pie")
            .to_str().unwrap().to_string();
        let args = vec![Path::new("resources/no-pie")
            .to_str().unwrap().to_string()];

        return TargetProcess::new(filename, args);
    }

    #[test]
    fn test_new_cc() {
        let target = init_target();
        target.start().map( |_| {
            let ctrl = target.get_controller();

            let mut brk = Breakpoint::new(0x401126, Mode::OneShot);
            let res_set = brk.set(&ctrl);
            assert_eq!(res_set, Ok(()));

            let pid = ctrl.get_pid();

            let err = Error::CustomError("Could not resume".to_string());
            let resume = ctrl.resume(false).or(Err(err));

            assert_eq!(resume, Ok(()));

            let wait = target.wait(None);
            assert_eq!(wait, Ok(WaitStatus::Stopped(pid, SIGTRAP)));

            let res_clean = brk.clean_code(&ctrl);
            assert_eq!(res_clean, Ok(()));

            let res_load = ctrl.load_map_file();
            assert_eq!(res_load, Ok(()));

            let cc = CodeCache::new(ctrl.clone(), 100);
            let _ = cc.as_ref().map( |cc| {
                info!("Buffer at {:x}", cc.get_offset())
            });
            assert_eq!(cc.map( |_| () ), Ok(()));

            info!("Breakpoint reached and handled");

            // Breakpoint reached, now let's resume to terminate the program.
            let err2 = Error::CustomError("Could not resume".to_string());
            let resume2 = ctrl.resume(false).or(Err(err2));

            assert_eq!(resume2, Ok(()));

            let res = ctrl.wait(None);
            assert_eq!(res, Ok(WaitStatus::Exited(pid, 0)));
        });
    }

    #[test]
    fn test_read_write() {
        let target = init_target();
        target.start().map( |_| {
            let ctrl = target.get_controller();

            let mut brk = Breakpoint::new(0x401126, Mode::OneShot);
            let res_set = brk.set(&ctrl);
            assert_eq!(res_set, Ok(()));

            let res2 = ctrl.resume(false);
            assert_eq!(res2, Ok(()));

            let res3 = ctrl.wait(None);
            assert_eq!(res3, Ok(WaitStatus::Stopped(ctrl.get_pid(), SIGTRAP)));

            let res_clean = brk.clean_code(&ctrl);
            assert_eq!(res_clean, Ok(()));

            let res_load = ctrl.load_map_file();
            assert_eq!(res_load, Ok(()));

            let res = CodeCache::new(ctrl.clone(), 100).and_then( |mut cc| {
                cc.write_byte(0x12, 0)?;
                let res = cc.read_byte(0);
                assert_eq!(res, Ok(0x12));

                cc.write(&[0x01, 0x02, 0x03, 0x04, 0x05], 30)?;

                let mempath = format!("/proc/{}/mem", ctrl.get_pid());
                let fileres = File::open(mempath).and_then( |mut memfile| {
                    let mut buf = vec![0; 1];
                    memfile.seek(SeekFrom::Start(cc.get_offset() as u64))?;
                    memfile.read_exact(&mut buf)
                        .map( |_| assert_eq!(buf, vec![0x12]) )?;

                    let mut buf2 = vec![0; 5];
                    memfile.seek(SeekFrom::Start(
                            cc.get_offset() as u64 + 30))?;
                    memfile.read_exact(&mut buf2).map({ |_|
                        assert_eq!(buf2, vec![0x01, 0x02, 0x03, 0x04, 0x05])
                    })?;
                    Ok(())
                });

                assert!(fileres.is_ok());
                Ok(())
            });

            assert_eq!(res, Ok(()));

        });
    }
}
