extern crate sorry;

#[cfg(test)]
mod tests {
    use std::path::Path;

    use sorry::TargetProcess;
    use sorry::buffer::Buffer;
    use sorry::buffer::remote::RemoteBuffer;

    fn init_target() -> TargetProcess {
        let filename = Path::new("resources/no-pie")
            .to_str().unwrap().to_string();
        let args = vec![Path::new("resources/no-pie")
            .to_str().unwrap().to_string()];

        return TargetProcess::new(filename, args);
    }

    #[test]
    fn read_byte_remote_buffer() {
        let target = init_target();
        target.start();

        let remote = RemoteBuffer::new(target.get_pid(), 0x401126, 100);

        let mut byte = remote.read_byte(0);
        assert_eq!(byte, Ok(0x55));

        byte = remote.read_byte(21);
        assert_eq!(byte, Ok(0x5d));
    }

    #[test]
    fn read_multiple_bytes_remote_buffer() {
        let target = init_target();
        target.start();

        let remote = RemoteBuffer::new(target.get_pid(), 0x401126, 100);

        let mut bytes = remote.read(4, 0);
        assert_eq!(bytes, Ok([0x55, 0x48, 0x89, 0xe5].to_vec()));

        bytes = remote.read(1, 21);
        assert_eq!(bytes, Ok([0x5d].to_vec()));

        bytes = remote.read(0, 0);
        assert_eq!(bytes, Ok([].to_vec()));
    }

    #[test]
    fn write_multiple_bytes_remote_buffer() {
        let target = init_target();
        target.start();

        let mut remote = RemoteBuffer::new(target.get_pid(), 0x401126, 100);

        let write_res = remote.write(&[0x00, 0x00, 0x00, 0x00], 0);
        assert_eq!(write_res, Ok(()));
        let bytes = remote.read(4, 0);
        assert_eq!(bytes, Ok([0x00, 0x00, 0x00, 0x00].to_vec()));
    }

    #[test]
    fn write_byte_remote_buffer() {
        let target = init_target();
        target.start();

        let mut remote = RemoteBuffer::new(target.get_pid(), 0x401126, 100);

        let write_res = remote.write_byte(0x00, 0);
        assert_eq!(write_res, Ok(()));

        let bytes = remote.read_byte(0);
        assert_eq!(bytes, Ok(0x00));
    }
}
