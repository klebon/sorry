extern crate elf;

#[macro_use]
extern crate log;
extern crate simplelog;

extern crate nix;

extern crate sorry;


#[cfg(test)]
mod tests {
    use std::ffi::CString;
    use std::path::Path;
    use std::thread;
    use std::time;

    use elf::File;
    use elf::types::*;

    use nix::sys::signal::*;
    use nix::sys::wait::WaitStatus;

    use nix::unistd::*;

    use sorry::TargetProcess;
    use sorry::breakpoint::*;
    use sorry::error::Error;

    #[test]
    fn start_dummy() {
        let filename = Path::new("resources/dummy")
            .to_str().unwrap().to_string();
        let args = vec![Path::new("resources/dummy")
            .to_str().unwrap().to_string()];

        let target = TargetProcess::new(filename, args);
        let res = target.start();
        let pid = target.get_pid();

        assert_eq!(res, Some(pid));
    }

    #[test]
    fn start_unexisting_program() {
        let filename = "unexisting".to_string();
        let args: Vec<String> = vec![];
        let target = TargetProcess::new(filename, args);
        let res = target.start();

        assert_eq!(res, None);
    }

    #[test]
    fn attach_with_pid() {
        match fork() {
            Ok(ForkResult::Parent { child, .. }) => {
                let dur = time::Duration::from_millis(1000);
                thread::sleep(dur);

                let res = TargetProcess::from_pid(child.as_raw());
                assert!(res.is_ok());

                let target = res.unwrap();
                let pid = target.get_pid();
                let ehdr = target.get_target_metadata().ehdr;

                let ctrl = target.get_controller();
                let exec = ctrl.get_executable_name();

                let loop_name = std::fs::canonicalize("resources/loop")
                    .map( |path| path.to_string_lossy().to_string() )
                    .unwrap_or(String::new());

                assert_eq!(exec, loop_name);

                if let Ok(ls_file) = File::open_path("resources/loop") {
                    assert_eq!(ehdr, ls_file.ehdr);
                }

                else {
                    assert!(false);
                }

                assert_eq!(child, pid);
            }

            Ok(ForkResult::Child) => {
                let file = "resources/loop".as_bytes();
                let cfile = CString::new(file).unwrap();
                let _ = execv(&cfile, &[
                              &cfile.clone(),
                              &CString::new("test").unwrap()
                ]);
            }

            Err(_) => {
                assert!(false);
            }
        }
    }

    #[test]
    fn get_all_symbols() {
        let filename = Path::new("resources/nolibs")
            .to_str().unwrap().to_string();
        let args = vec![Path::new("resources/nolibs")
            .to_str().unwrap().to_string()];

        let target = TargetProcess::new(filename, args);
        let syms = target.get_all_symbols();

        let expected = vec!["_start", "main"];
        let funcs: Vec<String> = syms.into_iter()
            .filter(|s| s.symtype == STT_FUNC)
            .map(|s| s.name).collect();

        // TODO: test more than only the local functions
        assert_eq!(funcs, expected);
    }

    #[test]
    fn find_symbol() {
        let filename = Path::new("resources/nolibs")
            .to_str().unwrap().to_string();
        let args = vec![Path::new("resources/nolibs")
            .to_str().unwrap().to_string()];

        let target = TargetProcess::new(filename, args);
        let res_section = target.get_target_metadata().get_section(".symtab");

        assert_eq!(res_section.is_some(), true);

        if let Some(symtab) = res_section {
            let main = target.find_symbol("main", symtab);
            assert_eq!(main.is_some(), true);
        }
    }

    #[test]
    fn get_dummy_main_function_offset() {
        let filename = Path::new("resources/dummy")
            .to_str().unwrap().to_string();
        let args = vec![Path::new("resources/dummy")
            .to_str().unwrap().to_string()];

        let target = TargetProcess::new(filename, args);
        let main_res = target.get_main_function_offset();

        assert_eq!(main_res.is_ok(), true);
        let _ = main_res.map( |addr| assert_eq!(addr, 0x1139) );
    }

    #[test]
    fn get_nolibs_main_function_offset() {
        let filename = Path::new("resources/nolibs")
            .to_str().unwrap().to_string();
        let args = vec![Path::new("resources/nolibs")
            .to_str().unwrap().to_string()];

        let target = TargetProcess::new(filename, args);
        let main_res = target.get_main_function_offset();

        assert_eq!(main_res.is_ok(), true);
        let _ = main_res.map( |addr| assert_eq!(addr, 0x1000) );
    }

    // resrouces/stripped is resources/dummy.c compiled with -s to remove the symbols.
    #[test]
    fn get_stripped_main_function_offset() {
        let filename = Path::new("resources/stripped")
            .to_str().unwrap().to_string();
        let args = vec![Path::new("resources/stripped")
            .to_str().unwrap().to_string()];

        let target = TargetProcess::new(filename, args);
        let main_res = target.get_main_function_offset();

        assert_eq!(main_res.is_ok(), true);
        let _ = main_res.map( |addr| assert_eq!(addr, 0x1139) );
    }

    #[test]
    fn test_resume() {
        let filename = Path::new("resources/no-pie")
            .to_str().unwrap().to_string();
        let args = vec![Path::new("resources/no-pie")
            .to_str().unwrap().to_string()];

        let target = TargetProcess::new(filename, args);
        target.start();

        let do_res = target.controller_do( |ctrl| {
            let res = ctrl.resume(false)
                .or(Err(Error::CustomError("Could not resume".to_string())) );
            assert!(res.is_ok());

            let status = ctrl.wait(None);
            debug!("Wait status: {:?}", res);
            assert_eq!(status, Ok(WaitStatus::Exited(ctrl.get_pid(), 0)));

            Ok(())
        });

        assert_eq!(do_res, Ok(()));
    }

    fn find_main(filename: &str) {
        let filename = Path::new(&filename)
            .to_str().unwrap().to_string();
        let args = vec![Path::new(&filename)
            .to_str().unwrap().to_string()];

        let mut target = TargetProcess::new(filename, args);
        target.start().and_then( |_| -> Option<()> {
            let _ = target.get_main_function_offset().and_then( |offset| {
                let _ = target.controller_mut_do( |ref mut ctrl| {
                    let load = ctrl.get_absolute_load_address();
                    let address = offset + load;

                    let mut brk = Breakpoint::new(address, Mode::OneShot);
                    let res_set = brk.set(&ctrl);
                    assert_eq!(res_set, Ok(()));

                    let pid = ctrl.get_pid();
                    let res = ctrl.resume(false).and_then( |_| ctrl.wait(None));
                    assert_eq!(res, Ok(WaitStatus::Stopped(pid, SIGTRAP)));

                    brk.clean_code(&ctrl)?;
                    let regs = ctrl.get_registers()?;

                    debug!("RIP: 0x{:x}, Address: 0x{:x}", regs.rip, address);
                    assert_eq!(regs.rip, address);

                    Ok(())
                });

                Ok(())
            });

            Some(())
        });
    }

    #[test]
    fn test_find_main_pie() {
        find_main("resources/dummy");
    }

    #[test]
    fn test_find_main_stripped_pie() {
        find_main("resources/stripped");
    }

    fn find_libc(filename: &str) {
        let filename = Path::new(&filename)
            .to_str().unwrap().to_string();
        let args = vec![Path::new(&filename)
            .to_str().unwrap().to_string()];

        let target = TargetProcess::new(filename, args);
        target.start().and_then( |_| -> Option<()> {
            let _ = target.get_main_function_offset().and_then( |offset| {
                let _ = target.controller_do( |ctrl| {
                    let load = ctrl.get_absolute_load_address();
                    let address = offset + load;

                    let mut brk = Breakpoint::new(address, Mode::OneShot);
                    let res_set = brk.set(&ctrl);
                    assert_eq!(res_set, Ok(()));

                    let pid = ctrl.get_pid();
                    let res = ctrl.resume(false).and_then( |_| ctrl.wait(None));
                    assert_eq!(res, Ok(WaitStatus::Stopped(pid, SIGTRAP)));

                    ctrl.load_map_file()?;
                    match ctrl.get_libc_address_range() {
                        Some((low, up)) => {
                            debug!("{:x}-{:x}", low, up);
                            assert_ne!(low, 0);
                            assert_ne!(up, 0);
                        }
                        None => panic!("No libc was found")
                    }

                    Ok(())
                });

                Ok(())
            });

            Some(())
        });
    }

    #[test]
    fn test_find_libc_dummy() {
        find_libc("resources/dummy");
    }

    #[test]
    fn test_find_libc_stripped() {
        find_libc("resources/stripped");
    }

    #[test]
    fn test_find_memory_map() {
        let path = Path::new("resources/dummy");
        let filename = path
            .to_str().unwrap().to_string();
        let args = vec![Path::new("resources/dummy")
            .to_str().unwrap().to_string()];

        let target = TargetProcess::new(filename, args);
        target.start();

        let _ = path.canonicalize().map( |name| {
            let filename = name.to_str().unwrap_or_default();
            let ctrl = target.get_controller();

            let entry_opt = ctrl.find_memory_map_entry(|entry| {
                entry.filename == Some(filename.to_string())
            });
            assert!(entry_opt.is_some());

            let entry = entry_opt.unwrap();
            assert_eq!(entry.filename, Some(filename.to_string()));
        });
    }
}
