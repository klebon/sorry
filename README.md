Sorry
=====

Sorry is a framework for Dynamic Binary Analysis written in Rust and based on
the [ptrace](http://man7.org/linux/man-pages/man2/ptrace.2.html) system call.

Requirements
------------

Sorry uses some other crates :

+ [capstone](https://docs.rs/capstone/0.6.0/capstone/): `0.6`
+ [elf](https://docs.rs/elf/0.0.10/elf/): `0.0.10`
+ [log](https://docs.rs/log/0.4.6/log/): `0.4`
+ [nix](https://docs.rs/nix/0.13.0/nix/): `0.13`
+ [simplelog](https://docs.rs/simplelog/0.5.3/simplelog/): `0.5`

Example
-------

```rust
use nix::sys::signal::*;
use nix::sys::wait::*;

use sorry::*;
use sorry::buffer::Buffer;
use sorry::buffer::codecache::CodeCache;
use sorry::error::Error;

fn main() {
    let executable = "resources/no-pie".to_string();
    let args = vec![executable.clone()];

    let mut target = TargetProcess::new(executable, args);
    target.start().map( |_| () );

    target.get_controller()
        .set_breakpoint(0x401126, move |ctrl, _| ctrl.load_map_file() );

    target.get_controller().resume(false);
    target.wait(None);
    CodeCache::new(target.get_controller().clone(), 100);

    new_res.map( |mut cc| {
        // mov $42, %edi
        // mov $60, %rax
        // syscall
        let exit42 = &[
            0xbf, 0x2a, 0, 0, 0,
            0x48, 0xc7, 0xc0, 0x3c, 0, 0, 0,
            0x0f, 0x05
        ];

        cc.write(exit42, 0);

        target.controller_do( |ctrl| {
            let mut regs = ctrl.get_registers()?;
            regs.rax = cc.get_offset() as u64;

            ctrl.inject_abs_call(regs.rip)?;

            ctrl.set_registers(regs)?;
            ctrl.resume(false).or_else( |err| {
                let msg = format!("{:?}", err);
                Err(Error::CustomError(msg.to_string()))
            })?;
            Ok(())
        });

        target.wait(None);
    });
}
```
